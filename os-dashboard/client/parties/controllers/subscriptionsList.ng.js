'use strict';

/**
 * @ngdoc controller
 * @name HitCursos.controllers.lesson
 * @module HitCursos
 * @description
 * _Reponsavel pelo lesson._
 *
 * @requires $scope
 */
(function () {

	var controller = function ($scope, $meteor, $window, $reactive) {

		var self = this;
    $reactive(self).attach($scope);

		var _this  = {

      /*
			 * Constructor method
			 * @param
			 * @return
			 * */
			main : function(){

        self.helpers({

         relevant_subscriptions_view: function() {
            return Subscriptions.find({});
          },

					ls_lita: function() {
						 return Commands.find({});
					 }

        });

        self.subscribe('relevant_subscriptions', () => {
         return [
            self.getReactively('searchText')
         ];
        });
			},

			/*
			 * @methods runLs
			 */
			runLs : function(){

        self.subscribe('ls', () => {
         return [
              "ls"
            ];
        });
			},

      /**
       * @methods add
       * @param obj
       */
      add: function(obj){

        Subscriptions.insert({
           name: obj.name,
           description: obj.description,
           createdAt: new Date()
         });
      },

			/**
			 * @methods remove
			 * @param obj
			 */
			remove: function(obj){

        Subscriptions.remove(obj._id);

			},

      /**
			 * @methods removeAll
			 */
			removeAll: function(){
          Meteor.call("Subscriptions.removeAll");
			},
			
		};

    _this.main();
    angular.extend(this, _this);

	};

	angular.module('socially')
		.controller('SubscriptionsListCtrl', controller);
}());
