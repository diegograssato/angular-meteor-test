Meteor.startup(function () {
 // Fixures
  if (Subscriptions.find().count() === 0) {
    var subscriptions = [
      {'name': 'jnjlegosdelta',
        'description': 'Fast just got faster with Nexus S.', 'createdAt': new Date()},
      {'name': 'jnjlegosbeta',
        'description': 'Get it on!', 'createdAt': new Date()},
      {'name': 'jnjlegoszeta',
        'description': 'Leisure suit required. And only fiercest manners.', 'createdAt': new Date()}
    ];

    for (var i = 0; i < subscriptions.length; i++)
      Subscriptions.insert({name: subscriptions[i].name, description: subscriptions[i].description, createdAt: subscriptions[i].createdAt});
  }
});


if ( Meteor.isServer ) {

  Meteor.publish("relevant_subscriptions", function (name) {

      if (typeof name == "string"){
        return Subscriptions.find({ name: { $regex: name } } );
      }else{
        return Subscriptions.find({})
      }

  });


  Meteor.publish("ls", function (x) {
     var self = this;
      var r = shell.pwd();
      shell.echo(r);
      shell.echo(x);
      var i = 0;
      Commands.remove({});
      shell.ls('*.js').forEach(function(file) {

        shell.echo(file);

        Commands.insert({
          file
        });

      });

      return Commands.find({});
  });


}
