#
# OS Dashboard
#
#  docker run --name dashboard -p 27017:27017 -p 28017:28017 -p 3000:3000 -v $(pwd):/projects 85c1faf8d66f
#
# VERSION               0.1.0

FROM ubuntu:14.04
MAINTAINER Diego Pereira Grassato <diego.grassato@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

RUN echo "deb http://archive.ubuntu.com/ubuntu trusty main restricted universe multiverse" > /etc/apt/sources.list
RUN echo "deb http://archive.ubuntu.com/ubuntu trusty-updates main restricted universe multiverse" >> /etc/apt/sources.list
RUN echo "deb http://security.ubuntu.com/ubuntu/ trusty-security main restricted universe multiverse" >> /etc/apt/sources.list
RUN echo "deb http://archive.canonical.com/ubuntu/ trusty partner" >> /etc/apt/sources.list
RUN echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
RUN echo "deb http://ppa.launchpad.net/chris-lea/node.js/ubuntu trusty main" >> /etc/apt/sources.list && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-key 136221EE520DDFAF0A905689B9316A7BC7917B12 && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927

RUN apt-get update -qq
#RUN apt-get -y dist-upgrade
RUN apt-get -y install vim curl git python ruby mongodb-org g++ build-essential make gcc nodejs language-pack-en-base python-software-properties
RUN LC_ALL=en_US.UTF-8 curl -sL https://install.meteor.com | sed s/--progress-bar/-sL/g | /bin/sh

RUN apt-get autoremove -y
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN mkdir -p -m 777 /projects
RUN sed -i "/bindIp/d" /etc/mongod.conf
RUN cd /opt && git clone --depth=1 https://github.com/TylerBrock/mongo-hacker.git \
 && rm -rf ~/.mongorc.js \
 && cd mongo-hacker \
 && make install

# Forward request and error logs to docker log collector
RUN ln -sf /dev/stderr /var/log/mongodb/mongod.log

# Define mountable directories.
VOLUME ["/var/lib/mongodb"]

# Expose ports.
#   - 27017: process
#   - 28017: http
EXPOSE 27017 28017 3000
ENV PORT 3000
ENV ROOT_URL "http://0.0.0.0:3000"
ENV MONGO_URL "mongodb://127.0.0.1:27017/meteor"

# Define default command.
ENTRYPOINT ["/usr/bin/mongod", "--config", "/etc/mongod.conf"]
CMD ["--quiet"]
WORKDIR /projects
