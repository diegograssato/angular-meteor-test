Subscriptions = new Mongo.Collection("subscriptions");
Commands = new Mongo.Collection("commands");

Subscriptions.allow({

  insert: function (userId, party) {
    /* user and doc checks ,
    return true to allow insert */
    return true;
  },
  update: function (userId, party, fields, modifier) {
    /* user and doc checks ,
    return true to allow insert */
    return true;
  },
  remove: function (userId, party) {
    /* user and doc checks ,
    return true to allow insert */
    return true;
  }
});

//Create private methods
Meteor.methods({
  'Subscriptions.removeAll': function() {
    Subscriptions.remove({});
  }
});
