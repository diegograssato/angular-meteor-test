angular.module("socially").controller("PartiesListCtrl", function ($scope, $meteor, $window, $reactive) {
$scope.parties = $meteor.collection(Parties).subscribe('parties');


  var self = this;



  $scope.remove = function (party) {
    $scope.parties.splice($scope.parties.indexOf(party), 1);
      $window.location.href = "/parties";
  };

  $scope.removeAll = function () {
    $scope.parties.remove();

    $window.location.href = "/parties";
  };

  $scope.runLs = function ()  {
    self.searchText = "beta";
  };

});
